"""Test cases for upt.restraint.wrapper.protocol module."""
import copy
from importlib.resources import files
import itertools
import os
import pathlib
import shlex
import unittest
from unittest import mock

from twisted.internet.error import ProcessExitedAlready
from twisted.internet.error import ReactorNotRunning

from tests.const import ASSETS_DIR
from tests.utils import create_temporary_files
from upt.logger import COLORS
from upt.plumbing.format import ProvisionData
from upt.restraint.file import RestraintJob
from upt.restraint.file import RestraintTask
from upt.restraint.wrapper.actions import ActionOnResult
from upt.restraint.wrapper.dataclasses import ConsoleTask
from upt.restraint.wrapper.dataclasses import TaskExecutionEnvironment
from upt.restraint.wrapper.host import RestraintHost
from upt.restraint.wrapper.protocol import RestraintClientProcessProtocol
from upt.restraint.wrapper.task_result import TaskResult

ASSETS = files(__package__) / 'assets/restraint_execution_states'

# pylint: disable=no-self-use,protected-access


class TestRestraintProtocol(unittest.TestCase):
    # pylint: disable=too-many-instance-attributes,too-many-public-methods
    """Ensure RestraintClientProcessProtocol works."""

    @mock.patch('upt.restraint.wrapper.protocol.Watcher', mock.Mock())
    def setUp(self) -> None:
        self.std_kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a',
                           'dump': True}
        self.req_asset = os.path.join(ASSETS_DIR, 'req.yaml')

        self.xmlpath = pathlib.Path(ASSETS_DIR, 'restraint_xml')
        self.xml = self.xmlpath.read_text()

        self.mock_rs_open = mock.patch('upt.restraint.wrapper.shell.open', create=True)
        self.mock_rs_open.start()

        self.beaker = ProvisionData.deserialize_file(self.req_asset).get_provisioner('beaker')
        kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a', 'dump': False}
        self.proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0],
                                                    lambda *args: None, None, **kwargs)
        for i, host in enumerate(self.proto.resource_group.recipeset.hosts, 1):
            host.counter = mock.Mock()
            host.counter.path = f'results_{i:04d}'

        self.proto.run_suffix = 'job.01'
        self.restraint_task = self.proto.wrap.run_restraint_job.recipesets[0].recipes[0].tasks[0]
        self.console_task = ConsoleTask(task_id=1, task_name='a3',
                                        result='PASS', status='Completed')
        self.host = self.proto.resource_group.recipeset.hosts[0]

    def tearDown(self) -> None:
        self.mock_rs_open.stop()

    def test_set_rerun(self):
        """Setting reruns."""
        resource_group = self.beaker.rgs[0]
        single_host = [copy.deepcopy(resource_group.recipeset.hosts[0])]
        multi_hosts = copy.deepcopy(resource_group.recipeset.hosts)
        self.std_kwargs['dump'] = False
        cases = (
            ('Single host with no rerun', 0, single_host, 0),
            ('Single host with multiples reruns', 2, single_host, 2),
            ('Multiple host with no rerun', 1, multi_hosts, 0),
            ('Multiple host with multiples reruns', 2, multi_hosts, 0),
        )
        for description, reruns, hosts, expected in cases:
            with self.subTest(description), mock.patch('upt.logger.LOGGER.info') as mock_logger:
                # Set hosts
                resource_group.recipeset.hosts = hosts
                # Set reruns
                self.std_kwargs['reruns'] = reruns
                proto = RestraintClientProcessProtocol(self.beaker, resource_group,
                                                       lambda *args: None, None, **self.std_kwargs)

                self.assertEqual(expected, proto.reruns)
                if len(hosts) > 1:
                    self.assertTrue(mock_logger.called)
                else:
                    self.assertFalse(mock_logger.called)

    @mock.patch('upt.logger.LOGGER.error')
    def test_get_restraint_xml_task(self, mock_error):
        """Ensure get_restraint_xml_task works."""
        # Currently we're testing when a recipe has previous rerun, because
        # it's very complicated due to actual coupling of protocol class.
        restraint_content = pathlib.Path(ASSETS_DIR, 'cki_restraint_with_ids.xml').read_text(
            encoding='utf-8'
        )
        restraint_job = RestraintJob.create_from_string(restraint_content)
        cases = (
            ('Get a existing task in the recipe getting info with running restraint',
             restraint_job, 123, 1, RestraintTask, []),
            ('Get a existing task in the recipe getting info without running restraint',
             None, 123, 1, RestraintTask, []),
            ('Get a non existing task (out of range) with running restraint',
             restraint_job, 123, 200, None,
             ['task T:%i not found in the recipe %i!', 200, 123]),
            ('Get a non existing task (out of range) without running restraint',
             None, 123, 200, None,
             ['task T:%i not found in the recipe %i!', 200, 123]),
        )
        for description, running_restraint, recipe_id, task_id, expected_type, logger_args in cases:
            mock_error.reset_mock()
            with self.subTest(description), \
                    mock.patch.object(self.proto.watcher,
                                      'get_job',
                                      return_value=running_restraint):
                result = self.proto.get_restraint_xml_task(recipe_id, task_id)
                if expected_type:
                    self.assertIsInstance(result, expected_type)
                else:
                    self.assertIsNone(result)
                if logger_args:
                    mock_error.assert_called_with(*logger_args)

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.reactor')
    @mock.patch('upt.restraint.wrapper.protocol.misc.enter_dir')
    def test_start_all(self, mock_enter, mock_reactor):
        """Ensure start_all calls what it should."""
        mock_enter.return_value.__enter__ = mock.Mock()
        mock_reactor.spawnProcess.return_value = 'spawned'
        mock_reactor.addSystemEventTrigger.return_value = 'trigger-set'

        self.proto.start_all()

        mock_reactor.spawnProcess.assert_called_with(self.proto,
                                                     self.proto.restraint_binary,
                                                     shlex.split(
                                                         self.proto.wrap.restraint_commands),
                                                     env=os.environ)
        mock_reactor.addSystemEventTrigger.assert_called()

    @mock.patch('upt.restraint.wrapper.watcher.Semaphore', mock.Mock())
    def test_filter_output(self):
        """Ensure filter_output works."""
        # Check that identify_host extracts hostname + recipe_id.
        self.assertIsNone(self.proto.filter_output('Disconnected..'))
        self.assertIsNone(self.proto.filter_output(
            'Connecting to host: root@hostname1, recipe id:123'))
        rst_host = self.proto.rst_hosts[0]
        self.assertEqual(rst_host.hostname, 'hostname1')
        self.assertEqual(rst_host.recipe_id, 123)

        self.assertIsNone(self.proto.filter_output(
            'Connecting to host: root@a.b.com, recipe id:123'))

    def test_identify_host(self):
        """Ensure identify_host works."""
        self.assertTrue(self.proto.identify_host(
            'Connecting to host: root@a.b.com, recipe id:1234'))
        self.assertFalse(self.proto.identify_host('invalid line'))

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.restraint.wrapper.protocol.twisted_task')
    @mock.patch('upt.restraint.wrapper.protocol.defer')
    def test_out_received(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived works."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data\n')
            mock_debug.assert_called_with('some data\n')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_find.assert_called_with('some data')
            mock_defer.Deferred.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.restraint.wrapper.protocol.twisted_task')
    @mock.patch('upt.restraint.wrapper.protocol.defer')
    def test_out_received_incomplete(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived doesn't process incomplete lines."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data')
            mock_debug.assert_called_with('some data')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_find.assert_not_called()
            mock_defer.Deferred.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.restraint.wrapper.protocol.twisted_task')
    @mock.patch('upt.restraint.wrapper.protocol.defer')
    def test_out_received_extra_part(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived handles extra partial lines correctly."""
        with mock.patch.object(self.proto, 'find_host_lines') as mock_find:
            self.proto.outReceived(b'some data\nextra')
            mock_debug.assert_called_with('some data\nextra')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_find.assert_called_with('some data')
            mock_defer.Deferred.assert_called()
            self.assertEqual(self.proto.unprocessed_out_data, 'extra')

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.restraint.wrapper.protocol.twisted_task')
    @mock.patch('upt.restraint.wrapper.protocol.defer')
    def test_out_received_disconnect(self, mock_defer, mock_task, mock_debug):
        """Ensure outReceived detects disconnects and calls host_heartbeat."""
        with mock.patch.object(self.proto, 'host_heartbeat') as mock_heartbeat:
            self.proto.outReceived(b'Disconnected.. delaying 60 seconds. Retry 15/15.\n')
            mock_debug.assert_called_with('Disconnected.. delaying 60 seconds. Retry 15/15.\n')

            mock_task.LoopingCall.assert_called_with(self.proto.host_heartbeat)
            mock_heartbeat.assert_called()
            mock_defer.Deferred.assert_called()

    @mock.patch('upt.logger.LOGGER.warning')
    def test_handle_failure(self, mock_warn):
        """Ensure handle_failure works."""
        mock_failure = mock.Mock()

        with mock.patch.object(self.proto, 'proc') as mock_proc:
            self.proto.handle_failure(mock_failure)
            mock_proc.signalProcess.assert_called_with('TERM')
            mock_warn.assert_called_with("Fatal exception caught %s", mock_failure.getTraceback())

        with mock.patch.object(self.proto, 'proc') as mock_proc:
            mock_proc.signalProcess.side_effect = itertools.chain([ProcessExitedAlready()],
                                                                  itertools.cycle([None]))
            self.proto.handle_failure(mock_failure)

        # Host died
        with mock.patch.object(self.proto, 'proc') as mock_proc:
            self.proto.handle_failure()
            mock_warn.assert_called_with('Host died, killing restraint process!')

    @mock.patch('upt.logger.LOGGER.print_result_in_color')
    @mock.patch('upt.logger.LOGGER.debug')
    def test_check_stderr(self, mock_debug, mock_color):
        """Ensure _check_stderr detects EWD hit and executes an action."""
        restraint_content = pathlib.Path(ASSETS_DIR, 'cki_restraint_with_ids.xml').read_text(
            encoding='utf-8'
        )
        restraint_job = RestraintJob.create_from_string(restraint_content)
        task = restraint_job.get_task_by_id(self.host.recipe_id, 1)
        with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
            # The mock below is pretty important: with no prior
            # results, we assume the task aborted.
            with mock.patch.object(self.host, 'task_results', []):
                mock_sync.return_value.__enter__.return_value.name = 'whatever'
                with mock.patch.object(self.proto, 'get_restraint_xml_task',
                                       return_value=task):
                    self.proto._check_stderr('Recipe 123 exceeded lab watchdog timer')

        self.assertIn(123, self.proto.recipe_ids_dead)
        mock_color.assert_any_call('*** Recipe 123 hit lab watchdog!')

        msg2 = ActionOnResult.format_msg(123, 1,
                                         'recipe is done processing [mark_recipe_done]', '*',
                                         'SELinux Custom Module Setup')
        mock_debug.assert_called_with(msg2)

    @mock.patch('upt.restraint.wrapper.protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error')
    def test_check_stderr_fatal(self, mock_error, mock_reactor):
        """Ensure _check_stderr works."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = True
        self.proto._check_stderr(err_indication)
        mock_error.assert_called_with("Exiting because: %s", err_indication)
        mock_reactor.callFromThread.assert_called()

    @mock.patch('upt.restraint.wrapper.protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_check_stderr_fatal_no_reactor(self, mock_reactor):
        """Ensure _check_stderr doesn't stop reactor when it's not running."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = False
        self.proto._check_stderr(err_indication)
        mock_reactor.callFromThread.assert_not_called()

    @mock.patch('upt.restraint.wrapper.protocol.reactor')
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_check_stderr_fatal_exc_handled(self, mock_reactor):
        """Ensure _check_stderr handles reactor exception."""
        err_indication = 'Could not resolve hostname'
        mock_reactor.running = True
        mock_reactor.callFromThread.side_effect = itertools.chain(
            [ReactorNotRunning()], itertools.cycle([None]))

        self.proto._check_stderr(err_indication)
        mock_reactor.callFromThread.assert_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.restraint.wrapper.protocol.defer')
    def test_err_received(self, mock_defer, mock_debug):
        """Ensure errReceived works."""
        self.proto.errReceived(b'some error')
        mock_debug.assert_called_with('some error')
        mock_defer.Deferred.assert_called()

        self.proto.errReceived(b'')

    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.identify_host',
                lambda *a: False)
    @mock.patch('upt.restraint.wrapper.protocol.reactor')
    def test_filter_output_with_path(self, mock_reactor):
        """Ensure filter_output works."""
        result = self.proto.filter_output('Using ./job.01 for job run')

        path2watch = os.path.join(f'{os.path.abspath(self.proto.kwargs["output"])}',
                                  os.path.relpath(self.proto.run_suffix))

        mock_reactor.callInThread.assert_called_with(self.proto.watcher.run, path2watch)
        self.assertIsNone(result)

    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.identify_host',
                lambda *a: False)
    @mock.patch('upt.restraint.wrapper.protocol.reactor', mock.Mock())
    def test_filter_output_host_line(self):
        """Ensure filter_output process host lines."""
        line = '[aaaa-bbbbbb-01.ccccc] T:       1 [test1             ] Running'
        result = self.proto.filter_output(line)

        self.assertEqual(
            result, [('aaaa-bbbbbb-01.ccccc', 'T:       1 [test1             ] Running')])

        self.assertIsNone(self.proto.filter_output('err'))

    def test_find_host_lines(self):
        """
        Ensure find_host_lines works.

        This function triggers all task and result checks, this will be changed soon.
        """
        # Create new protocol without watcher mocked.
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0],
                                               lambda *args: None, mock.Mock(),
                                               **self.std_kwargs)
        proto.run_suffix = 'job.01'

        line = '[aaaa-bbbbbb-01.ccccc] T:       1 [test1             ] Running'
        restraint_content = pathlib.Path(ASSETS_DIR, 'cki_restraint_with_ids.xml').read_text(
            encoding='utf-8'
        )
        restraint_job = RestraintJob.create_from_string(restraint_content)

        with mock.patch.object(proto.watcher, 'get_job', return_value=restraint_job):

            self.assertIsNone(proto.find_host_lines('blah'))
            proto.rst_hosts.append(RestraintHost('aaaa-bbbbbb-01.ccccc', 123))

            self.assertIsNone(proto.find_host_lines(line))

    @mock.patch('upt.logger.LOGGER.info')
    def test_find_host_lines_fail(self, mock_info):
        """Ensure find_host_lines warns on suspicious output."""
        self.proto.rst_hosts.append(RestraintHost('hh', 1234))
        line = '[aa] T:       1 [test1             ] Running'
        self.proto.find_host_lines(line)
        mock_info.assert_any_call('* line: "%s"', 'T:       1 [test1             ] Running')
        mock_info.assert_any_call('!!! Received line from unknown host %s', 'aa')

    def test_find_host_lines_abort(self):
        """Ensure find_host_lines ignores Aborted from restraint-client over panic from Beaker."""
        self.proto.rst_hosts.append(RestraintHost('aa', 123))
        line = '[aa] T:       1 [test1             ] Running\n' \
               '[aa] T:       2 [test1             ] Aborted\n'
        with mock.patch.object(self.proto, 'recipe_ids_dead', {123}):
            with mock.patch.object(self.proto, 'host_heartbeat', return_value={123}):
                with mock.patch.object(self.proto, 'process_task') as mock_process_task:
                    self.proto.find_host_lines(line)
                    # find_host_lines has to avoid processing that 'Aborted' output from restraint
                    # client to prioritize issue found by host_heartbeat, thus we get only one call
                    # instead of two.
                    assert mock_process_task.call_count == 1

    def test_split_hostline_match_valid(self):
        """Ensure split_hostline_match matches a valid line."""
        expected_host = self.proto.rst_hosts[0]
        expected = (expected_host, self.console_task)

        result = self.proto.split_hostline_match(
            'hostname1', '[hostname1] T:       1 [a3 ] Completed: PASS')

        self.assertEqual(expected, result)

    def test_split_hostline_match_invalid(self):
        """Ensure split_hostline_match handles invalid data."""
        result = self.proto.split_hostline_match(
            'hostname1', '[hostname1] T:       1a [test1 ] Running: PASS')

        self.assertIsNone(result)

    @mock.patch('builtins.print')
    def test_split_hostline_match_port(self, mock_print):
        """Ensure split_hostline_match prints restraint port."""
        self.proto.split_hostline_match('hostname1', '[hostname1] Listening on '
                                                     'http://localhost:36351')
        mock_print.assert_called_with('hostname1                                '
                                      '[hostname1] Listening on http://localhost:36351')

    @mock.patch('upt.logger.LOGGER.debug')
    def test_cleanup_handler_no_repeat(self, mock_debug):
        """Ensure cleanup_handler works and isn't called twice."""
        with mock.patch.object(self.proto, 'cleanup_done', True):
            self.proto.cleanup_handler()
            mock_debug.assert_not_called()

    @mock.patch('upt.logger.LOGGER.debug')
    @mock.patch('upt.restraint.wrapper.protocol.Watcher')
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'copy_results_and_logs')
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'evaluate_host_abort', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.attempt_heartbeat_stop', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.pathlib.Path', mock.Mock())
    def test_cleanup_handler(self, mock_recipe, mock_watcher, mock_debug):
        """Ensure cleanup_handler works and calls all it should."""
        kwargs = {'keycheck': 'no', 'reruns': 3, 'output': '/tmp/a',
                  'dump': False}
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               None, **kwargs)
        with mock.patch.object(proto.resource_group.recipeset.hosts[0], 'done_processing', True):
            with mock.patch.object(self.beaker, 'release_resources') as mock_release:
                proto.cleanup_handler()

        mock_watcher.return_value.observer.stop.assert_called()
        mock_debug.assert_called_with('restraint protocol cleanup runs (rid: %s)...',
                                      proto.resource_group.resource_id)

        mock_recipe.assert_called()
        mock_release.assert_called()

        self.assertTrue(proto.resource_group.recipeset.tests_finished)
        self.assertTrue(proto.cleanup_done)

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.Watcher', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'copy_results_and_logs')
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'evaluate_host_abort', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.attempt_heartbeat_stop')
    def test_cleanup_handler_rerun(self, mock_att_hrt_stop, mock_copy):
        """Ensure cleanup_handler handles reruns and adds protocols."""
        self.proto.resource_group.recipeset.waiting_for_rerun = True
        self.proto.reruns = 1

        with mock.patch.object(self.proto, 'add_protocol') as mock_add_protocol:
            self.proto.cleanup_handler()

            mock_add_protocol.assert_called()
            mock_copy.assert_called()

        self.assertTrue(self.proto.resource_group.recipeset.tests_finished)
        self.assertTrue(self.proto.cleanup_done)
        mock_att_hrt_stop.assert_called()

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.Watcher', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'copy_results_and_logs')
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'evaluate_host_abort', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.attempt_heartbeat_stop', mock.Mock())
    @mock.patch('upt.restraint.wrapper.protocol.traceback.print_exc')
    @mock.patch('upt.restraint.wrapper.protocol.RestraintClientProcessProtocol.'
                'dump_remaining_tasks_with_skip_status')
    def test_cleanup_handler_exception(self, mock_dump, mock_trace, mock_copy):
        """Ensure cleanup_handler handles exception from copy_results_and_logs."""
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0],
                                               lambda *args: None, None,
                                               **self.std_kwargs)

        mock_copy.side_effect = itertools.chain([FileNotFoundError()])
        with mock.patch.object(self.beaker, 'release_resources'):
            proto.cleanup_handler()
        mock_trace.assert_called()
        mock_dump.assert_called()

    @mock.patch('upt.logger.LOGGER.info')
    @mock.patch('builtins.print')
    def test_process_ended(self, mock_print, mock_info):
        """Ensure processEnded works."""
        with mock.patch.object(self.proto, 'cleanup_handler') as mock_cleanup_handler:
            reason = mock.MagicMock()
            self.proto.processEnded(reason)
            mock_cleanup_handler.assert_called()
            recipes = ['R:123', 'R:456', 'R:789']
            mock_info.assert_called_with('* restraint protocol (%s) %s retcode: %s reason: %s',
                                         recipes, 'ended.',
                                         reason.value.exitCode, reason.value)
            reason.value.__str__.return_value = 'ended by signal 13'
            reason.value.exitCode = 13
            self.proto.processEnded(reason)

            msg = f'{COLORS.RED}restraint protocol was killed by SIGPIPE, the test may be' \
                  f' misbehaving{COLORS.RESET}'
            mock_print.assert_any_call(msg)

    @mock.patch('upt.logger.LOGGER.info')
    def test_rerun_from_task(self, mock_info):
        """Ensure rerun_from_task works."""
        recipeset = self.beaker.rgs[0].recipeset
        task_result = TaskResult(self.host, self.restraint_task, self.console_task)

        rerun_kwargs = {'host': self.host, 'task_result': task_result, 'task_id': 1}
        self.proto.rerun_from_task(**rerun_kwargs)

        self.assertEqual(self.host.rerun_recipe_tasks_from_index, 1)
        self.assertEqual(recipeset.attempts_made, 0)

        msg = ActionOnResult.format_msg(self.host.recipe_id, 1,
                                        'Making a re-run attempt 1/3 for the entire recipeSet',
                                        '*', 'abc')
        mock_info.assert_called_with(msg)

    @mock.patch('upt.logger.LOGGER.info')
    def test_rerun_from_task_not_called(self, mock_info):
        """Ensure rerun_from_task doesn't do anything when the host is waiting for a rerun."""
        # Overwrite number of hosts to allow reruns
        self.beaker.rgs[0].recipeset.hosts = [self.beaker.rgs[0].recipeset.hosts[0]]
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0],
                                               lambda *args: None, None,
                                               **self.std_kwargs)

        host = self.beaker.rgs[0].recipeset.hosts[0]
        with mock.patch.object(self.beaker.rgs[0].recipeset, 'waiting_for_rerun', True):
            proto.rerun_from_task(**{'host': host})
            mock_info.assert_not_called()

    def test_add_task_result(self):
        """Ensure add_task_result works."""
        # Before to update the task results for the host should be empty
        self.assertEqual(0, len(self.host.task_results))

        task_result = self.proto.add_task_result(self.host, self.restraint_task,
                                                 self.console_task)
        self.assertIsInstance(task_result, TaskResult)

        # After task results should have one element
        self.assertEqual(1, len(self.host.task_results))

    @mock.patch('upt.logger.LOGGER.debug')
    def test_mark_recipe_done(self, mock_debug):
        """Ensure mark_recipe_done works."""
        task_result = TaskResult(self.host, self.restraint_task, self.console_task)

        with mock.patch.object(self.host, 'done_processing', True):
            self.proto.mark_recipe_done(**{'recipe_id': 123, 'task_result': task_result})
            mock_debug.assert_not_called()
        with mock.patch.object(self.host, 'done_processing', False):
            self.proto.mark_recipe_done(**{'recipe_id': 123, 'task_result': task_result})
            msg = ActionOnResult.format_msg(123, 1,
                                            'recipe is done processing [mark_recipe_done]',
                                            '*', 'abc')
            mock_debug.assert_called_with(msg)

    def test_mark_recipe_done_raises(self):
        """Ensure mark_recipe_done raises error on invalid data."""
        task_result = TaskResult(self.host, self.restraint_task, self.console_task)

        with self.assertRaises(RuntimeError):
            self.proto.mark_recipe_done(**{'recipe_id': 5678, 'task_result': task_result})

    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    def test_is_ewd_hit(self):
        """Ensure is_ewd_hit works."""
        with mock.patch.object(self.proto, 'recipe_ids_dead', {1234}):
            self.assertTrue(self.proto.is_ewd_hit(1234, 'Aborted'))

        self.assertFalse(self.proto.is_ewd_hit(1234, 'Aborted'))

    def test_prepare_actions(self):
        """Ensure prepare_actions works."""
        for item in self.proto._prepare_actions():
            self.assertIsInstance(item, ActionOnResult)

    def test_is_last_host_task(self):
        """Ensure is_last_host_task works."""
        self.assertTrue(self.proto.is_last_host_task(123, 2))

        with self.assertRaises(AssertionError):
            self.proto.is_last_host_task(756, 200)

    def test_evaluate_task_result(self):
        """Ensure evaluate_task_result works."""
        host0 = self.host
        host1 = copy.deepcopy(host0)
        host1.done_processing = True
        host1.recipe_id = 456

        resource_group = self.beaker.rgs[0]
        resource_group.recipeset.hosts = [host1, host0]
        proto = RestraintClientProcessProtocol(self.beaker, resource_group,
                                               lambda *args: None, mock.Mock(),
                                               **self.std_kwargs)
        self.console_task.result = 'WARN'
        task_result = TaskResult(host0, self.restraint_task, self.console_task)

        proto.evaluate_task_result(task_result, host0)
        proto.evaluate_task_result(task_result, host1)

    def test_evaluate_task_result_adapter(self):
        """Ensure evaluate_task_result works and calls ."""
        resource_group = self.beaker.rgs[0]
        host1 = copy.deepcopy(self.host)
        host1.done_processing = True
        host1.recipe_id = 456

        resource_group.recipeset.hosts = [host1, self.host]
        proto = RestraintClientProcessProtocol(self.beaker, resource_group,
                                               lambda *args: None, mock.Mock(),
                                               **self.std_kwargs)
        proto.run_suffix = 'job.01'

        self.console_task.result = 'Panic'
        self.console_task.status = 'Aborted'
        task_result = TaskResult(self.host, self.restraint_task, self.console_task)
        with mock.patch.object(proto, 'is_last_host_task', lambda *args: True):
            proto.evaluate_task_result(task_result, self.host)

            proto.evaluate_task_result(task_result, host1)
            # Don't dump on re-runs.
            proto.on_task_result.assert_called()

    @mock.patch('builtins.print')
    def test_evaluate_task_result_no_eval_setup(self, mock_print):
        """Ensure that evaluate_task_result will not try to evaluate setup task as test."""
        restraint_task = RestraintTask.create_from_string(
            '<task id="1" name="Setup task" result="PASS" status="Completed"></task>')
        proto = RestraintClientProcessProtocol(self.beaker, self.beaker.rgs[0], lambda *args: None,
                                               mock.Mock(), **self.std_kwargs)
        self.console_task.name = 'Setup task'
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        proto.evaluate_task_result(task_result, self.host)
        mock_print.assert_called_with('Setup task is a setup task - not evaluating as test.')

    @mock.patch('upt.logger.LOGGER.error')
    def test_process_task(self, mock_error):
        """Ensure process_task handles a corner case right."""
        restraint_content = pathlib.Path(ASSETS_DIR, 'cki_restraint_with_ids.xml').read_text(
            encoding='utf-8'
        )
        restraint_job = RestraintJob.create_from_string(restraint_content)
        restraint_task = restraint_job.get_task_by_id(self.host.recipe_id, 999)
        console_task = ConsoleTask(task_id=999, task_name='',
                                   status='Aborted', result='Panic')
        with mock.patch.object(self.proto, 'get_restraint_xml_task', return_value=restraint_task):
            self.assertIsNone(self.proto.process_task(self.host, console_task))
        mock_error.assert_called()

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.error', mock.Mock())
    def test_host_heartbeat_noop(self):
        """Ensure host_heartbeat does nothing when no host have issues."""
        with mock.patch.object(self.proto, 'watcher'):
            with mock.patch.object(self.proto.provisioner, 'heartbeat') as mock_heartbeat:
                with mock.patch.object(self.proto, 'recipe_ids_dead', {}):
                    self.proto.host_heartbeat()
                    mock_heartbeat.assert_called()

    @mock.patch('builtins.print', mock.Mock())
    @mock.patch('upt.logger.LOGGER.warning', mock.Mock())
    def test_host_heartbeat(self):
        """Ensure host_heartbeat works."""
        with (mock.patch.object(self.proto, 'watcher'),
              mock.patch.object(self.proto.provisioner, 'heartbeat'),
              mock.patch.object(self.proto, 'recipe_ids_dead', {456, 789}),
              mock.patch.object(self.proto, 'heartbeat_loop') as mock_loop,
              mock.patch.object(self.proto, 'process_task'),
              mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync,
              mock.patch.object(self.proto, 'handle_failure') as mock_failure):
            mock_sync.return_value.__enter__.return_value.name = 'whatever'
            mock_loop.stop.side_effect = itertools.chain([AssertionError()],
                                                         itertools.cycle([None]))
            self.proto.host_heartbeat()

            host = self.proto.resource_group.recipeset.hosts[1]
            self.assertEqual(host.task_results[0].status, 'Aborted')
            mock_loop.stop.assert_called()
            mock_failure.assert_called()

    @mock.patch('upt.logger.LOGGER.error')
    def test_evaluate_host_abort(self, mock_error):
        """Ensure evaluate_host_abort works."""
        with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
            mock_sync.return_value.__enter__.return_value.name = 'whatever'
            with mock.patch.object(self.proto.resource_group.recipeset.hosts[0],
                                   'task_results', []):
                with mock.patch.object(self.proto, 'get_restraint_xml_task',
                                       lambda *args: None):
                    self.proto.evaluate_host_abort(
                        self.proto.resource_group.recipeset.hosts[0])
                    mock_error.assert_called_with(
                        '💀 Unhandled case: cannot find task_id 1!')

    def test_evaluate_host_abort_prior_tasks(self):
        """Ensure evaluate_host_abort works for cases when some tasks were run."""
        restraint_task = RestraintTask.create_from_string(
            '<task id="10" name="t10" result="Warn" status="Aborted" />'
        )
        console_task = ConsoleTask(task_id=10, task_name='t10',
                                   result='Warn', status='Aborted')
        with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
            mock_sync.return_value.__enter__.return_value.name = 'whatever'

            with mock.patch.object(self.host, 'task_results', [TaskResult(self.host,
                                                                          restraint_task,
                                                                          console_task)]):
                with mock.patch.object(self.proto, 'get_restraint_xml_task',
                                       return_value=restraint_task):
                    self.proto.evaluate_host_abort(self.host)

    @mock.patch('upt.logger.LOGGER.debug')
    def test_boot_failure(self, mock_debug):
        """Ensure EWD during Boot test is marked as such."""
        # We have to use a task named "Boot test".
        xml_content = """
        <task name="Boot test" >
          <params>
            <param name="CKI_NAME" value="Boot test"/>
            <param name="CKI_UNIVERSAL_ID" value="Boot test"/>
          </params>
        </task>
        """
        restraint_task = RestraintTask.create_from_string(xml_content)
        self.console_task.result = 'Panic'
        self.console_task.status = 'Aborted'
        with mock.patch.object(self.proto, 'recipe_ids_dead', {123}):
            # It has to be a task of recipe_id 123, which hits EWD.
            ewd_hit = self.proto.is_ewd_hit(123, 'Panic')
            environment = TaskExecutionEnvironment(is_ewd_hit=ewd_hit,
                                                   is_multihost=self.proto.is_multihost,
                                                   restraint_output_path=pathlib.Path(
                                                       'artifacts/run.done.01/job.01'
                                                   ))
            task_result = TaskResult(self.host, restraint_task, self.console_task, environment)
            with mock.patch.object(self.proto, 'sem_lwd_sync') as mock_sync:
                mock_sync.return_value.__enter__.return_value.name = 'whatever'
                self.proto.evaluate_task_result(task_result, self.host)

                msg = ActionOnResult.format_msg(self.host.recipe_id, task_result.task_id,
                                                'recipe is done processing [mark_recipe_done]',
                                                '*',
                                                task_result.testname)
                mock_debug.assert_called_with(msg)

    def test_cond_print_notification(self):
        """Ensure no notification of failure and maintainer info is printed on pass/complete."""
        restraint_task = RestraintTask.create_from_string(
            '<task name="Boot test" id="1" result="PASS" status="Completed" />')

        self.console_task.task_name = 'Boot test'
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        RestraintClientProcessProtocol.cond_print_notification(task_result, [None, None])

    @mock.patch('upt.restraint.wrapper.protocol.pathlib.Path.read_text', mock.MagicMock())
    @mock.patch('upt.restraint.wrapper.protocol.RestraintJob', mock.MagicMock())
    @mock.patch('upt.restraint.wrapper.protocol.ET.tostring', lambda *args, **kwargs: 'foo')
    @mock.patch('upt.restraint.wrapper.protocol.ET', mock.MagicMock())
    def test_copy_results_and_logs(self):
        """Ensure copy_results_and_logs works and prints a warning when a directory is missing."""
        with create_temporary_files(
                [],
                # Create dirs that are present and method under test moves files to them.
                ['output/results_0001/',
                 'output/results_0002/',
                 'output/job.01/recipes/123/tasks/1/logs',
                 'output/job.01/recipes/456/tasks/1/logs',
                 'output/job.01/recipes/456/tasks/1/results/9876/logs',
                 'output/job.01/recipes/456/tasks/1/results/9876/results'
                 ]
        ):
            self.proto.dump = True
            self.proto.output = 'output'
            hosts = self.proto.resource_group.recipeset.hosts[:2]
            console_task = ConsoleTask(task_id=1, task_name='a2',
                                       result='Panic', status='Aborted')
            hosts[0].task_results.append(
                TaskResult(hosts[0],
                           RestraintTask.create_from_string('<task name="a1" />'),
                           console_task)
            )
            console_task = ConsoleTask(task_id=1, task_name='a2',
                                       result='Panic', status='Aborted')
            task_result = TaskResult(
                hosts[1],
                RestraintTask.create_from_string('<task name="a2" />'),
                console_task
            )
            hosts[1].task_results.append(task_result)
            self.proto.copy_results_and_logs(hosts)
            # Finally, check that files are actually there...
            self.assertTrue(pathlib.Path('output/results_0001/index.html').is_file())
            self.assertTrue(pathlib.Path('output/results_0002/index.html').is_file())
            self.assertTrue(pathlib.Path(
                'output/results_0001/LOGS_J:1234_0_R_123_T_1_test-redhat_a1').is_dir())
            self.assertTrue(pathlib.Path(
                'output/results_0002/LOGS_J:1234_0_R_456_T_1_test-redhat_a2').is_dir())
            self.assertTrue(pathlib.Path(
                'output/results_0002/RESULTS_J:1234_0_R_456_T_1_test-redhat_a2').is_dir())

            # review it
            with mock.patch.object(self.proto, 'run_suffix', 'xxx'):
                self.proto.copy_results_and_logs(hosts)

    def test_copy_results_and_logs_exception(self):
        """Ensure copy_results_and_logs handles exception from create_fixed_index."""
        with create_temporary_files([]):
            # Create dirs that are present and method under test moves files to them.
            self.proto.dump = True
            hosts = self.proto.resource_group.recipeset.hosts[:1]
            restraint_task = RestraintTask.create_from_string(
                '<task id="1" name="a3" result="Panic" status="Aborted" />'
            )
            self.console_task.result = 'Panic'
            self.console_task.status = 'Aborted'
            hosts[0].task_results.append(TaskResult(hosts[0],
                                                    restraint_task,
                                                    self.console_task))

            with mock.patch.object(self.proto, 'create_fixed_index') as mock_index:
                mock_index.side_effect = itertools.chain([FileNotFoundError()])

                self.proto.copy_results_and_logs(hosts)

    def test_dump_remaining_tasks_with_skip_status(self):
        """Ensure the remainining tasks have SKIP status after host processing."""
        restraint_task = RestraintTask.create_from_string(
            '<task name="Boot test" ><params>'
            '<param name="CKI_NAME" value="Boot test"/>'
            '</params></task>')
        restraint_second_task = RestraintTask.create_from_string(
            '<task name="Boot test" ><params>'
            '<param name="CKI_NAME" value="foo test"/>'
            '</params></task>')
        # Force an EWD in the Boot test.
        console_task = ConsoleTask(task_id=1, task_name='Boot test',
                                   result='Panic', status='Aborted')
        environment = TaskExecutionEnvironment(is_ewd_hit=True,
                                               is_multihost=False,
                                               restraint_output_path=pathlib.Path(
                                                   'artifacts/run.done.01/job.01'
                                               ))
        task_result = TaskResult(self.host, restraint_task, console_task, environment)
        task_result.kcidb_status = 'FAIL'
        console_task = ConsoleTask(task_id=2, task_name='CKI_NAME', status='', result='')
        second_task_result = TaskResult(self.host, restraint_second_task, console_task)
        self.host.planned_tests = [task_result, second_task_result]
        self.host.task_results = [task_result]
        with (
            mock.patch.object(self.proto, 'get_dumpfile_name', lambda *_, **__: ''),
            mock.patch.object(self.proto, 'on_task_result'),
        ):
            self.proto.dump_remaining_tasks_with_skip_status(self.host)
            self.assertEqual('Cancelled', second_task_result.status)
            self.assertEqual('SKIP', second_task_result.kcidb_status)

            # Ensure we didn't change status of tasks that already had it
            self.assertEqual('FAIL', task_result.kcidb_status)
