"""Test cases for task_result module."""
from importlib.resources import files
import pathlib
import unittest

from freezegun import freeze_time

from tests.utils import create_temporary_files
from upt.plumbing.objects import Host
from upt.restraint.file import RestraintTask
from upt.restraint.wrapper.dataclasses import ConsoleTask
from upt.restraint.wrapper.dataclasses import TaskExecutionEnvironment
from upt.restraint.wrapper.task_result import SubTaskResult
from upt.restraint.wrapper.task_result import TaskResult

ASSETS = files(__package__) / 'assets/restraint/wrapper/task_result'


class TestTaskResult(unittest.TestCase):
    """Test cases for task_result class."""

    @freeze_time("2023-05-15 20:26:58")
    def setUp(self) -> None:
        """
        Setup for this test case.

        In this class we need a RestraintTaskElement, this object
        could come from the original restraint file or from the running
        restraint file.

        That's why we have two different task result:
        * running_task_result gets the information from a running restraint file
        * original_task_result gets the information from the original restraint file
        """
        # Common section
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1})
        self.console_task = ConsoleTask(task_id=1, task_name='Boot test',
                                        result='Pass', status='Completed')
        self.environment = TaskExecutionEnvironment(is_ewd_hit=False,
                                                    is_multihost=False,
                                                    restraint_output_path=pathlib.Path(
                                                        'artifacts/run.done.01/job.01'
                                                    ))

        # Running restraint
        xml_content = (ASSETS / 'running_restraint_task.xml').read_text(encoding='utf-8')
        self.running_restraint_task = RestraintTask.create_from_string(xml_content)
        self.running_task_result = TaskResult(self.host, self.running_restraint_task,
                                              self.console_task, self.environment)

        # Original restraint
        xml_content = (ASSETS / 'original_restraint_task.xml').read_text(encoding='utf-8')
        self.original_restraint_task = RestraintTask.create_from_string(xml_content)
        self.original_task_result = TaskResult(self.host, self.original_restraint_task,
                                               self.console_task, self.environment)

    def test_test_maintainers(self):
        """
        Ensure TaskResult is created OK and test_maintainers are parsed.

        The required information is the original restraint,
        and running restraint never should change it.
        """
        expected = [{'name': 'Bruno Goncalves',
                     'email': 'bgoncalv@redhat.com',
                     'gitlab': 'bgoncalv'},
                    {'name': 'Jeff Bastian',
                     'email': 'jbastian@redhat.com',
                     'gitlab': 'jbastianrh'}]

        cases = (
            ('Check in original task result', self.original_task_result),
            ('Check in running task result', self.running_task_result),

        )

        for description, task_result in cases:
            with self.subTest(description):
                self.assertEqual(expected, task_result.test_maintainers)

    def test_output_files(self):
        """Ensure output_files can really find files in expected directories."""
        prefix_output_files = 'artifacts/run.done.01/job.01/recipes/1/tasks/1'
        common_files = [
            pathlib.Path(prefix_output_files, 'logs/harness.log'),
            pathlib.Path(prefix_output_files, 'logs/taskout.log'),
            pathlib.Path(prefix_output_files, 'logs/io_perf_base_kernel.log'),
            pathlib.Path(prefix_output_files, 'logs/kernel_config.log'),
            pathlib.Path(prefix_output_files, 'logs/io_perf_cki_kernel.log'),
            pathlib.Path(prefix_output_files, 'logs/test_console.log'),
        ]

        results_files = [
            pathlib.Path(prefix_output_files, 'results/1/logs/dmesg.log'),
            pathlib.Path(prefix_output_files, 'results/1/logs/avc.log'),
            pathlib.Path(prefix_output_files, 'results/2/logs/dmesg.log'),
            pathlib.Path(prefix_output_files, 'results/2/logs/avc.log'),
        ]

        all_files = common_files + results_files

        cases = (
            ('Running restraint task', self.running_task_result, common_files),
            ('Original restraint task', self.original_task_result, all_files),

        )
        for description, task_result, expected_files in cases:
            with self.subTest(description), create_temporary_files(all_files):
                self.assertCountEqual(expected_files, task_result.output_files)

    def test_is_cki_test_with_no_cki_test(self):
        """
        Ensure is_cki_test works when is not a cki test.

        The required information is the original restraint,
        and running restraint never should change it.
        """
        xml_content_without_anything = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_MAINTAINERS" value="abc de &lt;abcde@redhat.com&gt;"/>
            </params>
        </task>
        """

        xml_with_cki_name = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_NAME" value="a3"/>
            </params>
        </task>
        """

        xml_with_universal_id = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_UNIVERSAL_ID" value="1"/>
            </params>
        </task>
        """
        for xml_content in [xml_content_without_anything, xml_with_cki_name,
                            xml_with_universal_id]:
            restraint_task = RestraintTask.create_from_string(xml_content)
            task_result = TaskResult(self.host, restraint_task, self.console_task)
            self.assertFalse(task_result.is_cki_test)

    def test_is_cki_test_with_a_cki_test(self):
        """
        Ensure is_cki_test works when is a cki test.

        The required information is the original restraint,
        and running restraint never should change it.
        """

        xml_content = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_NAME" value="a3"/>
              <param name="CKI_UNIVERSAL_ID" value="1"/>
            </params>
        </task>
        """

        restraint_task = RestraintTask.create_from_string(xml_content)
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        self.assertTrue(task_result.is_cki_test)

    def test_task_result_is_boot_task(self):
        """
        Ensure task is_boot_task works when is a boot test.

        The required information is the original restraint,
        and running restraint never should change it.
        """
        xml_with_universal_id = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="CKI_UNIVERSAL_ID" value="boot"/>
            </params>
        </task>
        """

        xml_with_task_name = """
        <task id="1" name="Boot Test" status="Completed" result="PASS">
            <params>
              <param name="CKI_NAME" value="boot"/>
            </params>
        </task>
        """
        for xml_content in [xml_with_universal_id, xml_with_task_name]:
            restraint_task = RestraintTask.create_from_string(xml_content)
            task_result = TaskResult(self.host, restraint_task, self.console_task)
            self.assertTrue(task_result.is_boot_task)

    def test_task_result_is_not_boot_task(self):
        """Ensure task is_boot_task works when is not a boot test.

        The required information is the original restraint,
        and running restraint never should change it.
        """
        xml_content = """
        <task id="1" name="a3" status="Completed" result="PASS">
            <params>
              <param name="OTHER_VALUE" value="boot"/>
            </params>
        </task>
        """
        restraint_task = RestraintTask.create_from_string(xml_content)
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        self.assertFalse(task_result.is_boot_task)

    def test_task_result_calculate_result_with_skipped_results_without_result(self):
        """Check calculate result when we have a skipped result without result."""
        # ATM we're not looking in the RestraintTaskElement to get status and result
        xml_content = """
        <task id="1" name="a3" status="Completed" result="WARN">
           <results>
             <result id="1" path="/some/path" result="PASS">
             </result>
             <result id="2" path="/other/path" result="SKIP">
             </result>
           </results>
        </task>
        """
        expected = 'SKIP'
        restraint_task = RestraintTask.create_from_string(xml_content)
        self.console_task.result = ''
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        self.assertEqual(expected, task_result.result)

    def test_task_result_calculate_result_with_result(self):
        """Check calculate result when we have a with result."""
        # ATM we're not looking in the RestraintTaskElement to get status and result
        xml_content = """
        <task id="1" name="a3" status="Completed" result="FAIL">
           <results>
             <result id="1" path="/some/path" result="PASS">
             </result>
             <result id="2" path="/other/path" result="SKIP">
             </result>
           </results>
        </task>
        """
        expected = 'Aborted'
        restraint_task = RestraintTask.create_from_string(xml_content)
        self.console_task.result = 'Aborted'
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        self.assertEqual(expected, task_result.result)

    def test_task_result_calculate_result_when_status_is_not_completed_(self):
        """Check calculate result when we have a skipped result without result."""
        # ATM we're not looking in the RestraintTaskElement to get status and result
        xml_content = """
        <task id="1" name="a3" status="Running">
        </task>
        """
        expected = ''
        restraint_task = RestraintTask.create_from_string(xml_content)
        self.console_task.result = ''
        self.console_task.status = 'Running'
        task_result = TaskResult(self.host, restraint_task, self.console_task)
        self.assertEqual(expected, task_result.result)

    def test_start_time(self):
        """
        Check start time property.

        If we get the information from the restraint we will use, otherwise
        TaskResult will set the value when the object is initiliazed.
        """
        cases = (
            ('Information in the restraint task', self.running_task_result,
             '2022-05-06T20:26:58+00:00'),
            ('Information not in the restraint task', self.original_task_result,
             '2023-05-15T20:26:58+00:00'),
        )
        for description, task_result, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, task_result.start_time)

    def test_subtests_with_and_without_logs(self):
        """Only subtests with logs should be added."""
        xml_content = """
        <task id="1" name="Boot test" status="Completed" result="PASS">
           <results>
             <result id="1" path="subtest_with_logs" result="PASS">
               <logs>
                 <log path="recipes/1/tasks/1/results/1/logs/dmesg.log" filename="dmesg.log"/>
               </logs>
             </result>
             <result id="2" path="subtest_without_logs" result="PASS">
             </result>
           </results>
        </task>
        """
        restraint_task = RestraintTask.create_from_string(xml_content)
        task_result = TaskResult(self.host, restraint_task, self.console_task, self.environment)
        self.assertEqual(1, len(task_result.results))
        self.assertEqual('subtest_with_logs', task_result.results[0].name)

    def test_subtaskresult_list(self):
        """
        Check the list of subtaskresult.

        With running restraint task the list should have subtaskresults,
        with original restraint task the list should be empty.
        """
        cases = (
            ('Running restraint task', self.running_task_result, 2),
            ('Original restraint task', self.original_task_result, 0),
        )

        for description, task_result, expected in cases:
            with self.subTest(description):
                self.assertEqual(expected, len(task_result.results))


class TestSubTaskResult(unittest.TestCase):
    """Test cases for SubTaskResult class."""

    def setUp(self) -> None:
        """Setup for this test case."""
        # Running restraint
        xml_content = (ASSETS / 'running_restraint_task.xml').read_text(encoding='utf-8')
        restraint_task = RestraintTask.create_from_string(xml_content)
        self.task_output_path = \
            pathlib.Path('artifacts/run.done.01/job.01/recipes/1/tasks/1')
        self.subtaskresult = SubTaskResult(restraint_task.results[0], self.task_output_path)

    def test_class_attributes(self):
        """Test attributes."""
        relative_path = 'results/1/logs'
        expected_id = 1
        expected_name = 'distribution/kpkginstall/kernel-in-place'
        expected_files = [
            pathlib.Path(self.task_output_path, relative_path, 'dmesg.log'),
            pathlib.Path(self.task_output_path, relative_path, 'avc.log'),
        ]
        expected_status = 'PASS'

        self.assertEqual(expected_id, self.subtaskresult.result_id)
        self.assertEqual(expected_name, self.subtaskresult.name)
        with create_temporary_files(expected_files):
            self.assertCountEqual(expected_files, self.subtaskresult.output_files)
        self.assertEqual(expected_status, self.subtaskresult.status)
