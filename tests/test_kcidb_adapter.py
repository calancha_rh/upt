"""Test cases for kcidb_adapter module."""
from datetime import timedelta
import os
import pathlib
import unittest
from unittest import mock

from cki_lib.kcidb.file import KCIDBFile
from dateutil.parser import parse as date_parse
from freezegun import freeze_time

from tests.utils import KCIDB_DEFAULT_JSON
from tests.utils import create_temporary_kcidb
from upt.misc import OutputDirCounter
from upt.plumbing.objects import Host
from upt.restraint.file import RestraintRecipe
from upt.restraint.wrapper.dataclasses import ConsoleTask
from upt.restraint.wrapper.kcidb_adapter import KCIDBTestAdapter
from upt.restraint.wrapper.kcidb_adapter import RestraintStandaloneTest
from upt.restraint.wrapper.task_result import TaskResult

DEF_ENV_MOCK_DICT = {'CI_JOB_ID': '1234', 'CI_PIPELINE_ID': '5678', 'CI_PROJECT_PATH': 'path',
                     'CI_PROJECT_ID': '40', 'CI_SERVER_URL': 'https://', 'CI_JOB_STAGE': 'test',
                     'CI_JOB_URL': 'https://gitlab/job/1234', 'BEAKER_URL': 'https://beaker',
                     'CI_JOB_NAME': 'test x86_64', 'job_created_at': '2019-10-24T13:40:46.632298Z',
                     'CI_COMMIT_SHA': 'deadbeef',
                     'CI_PROJECT_DIR': 'whatever',
                     'GITLAB_CI': 'true',
                     'KCIDB_DUMPFILE_NAME': 'kcidb_all.json',
                     'CI_COMMIT_REF_NAME': 'block',
                     'REVISION_PATH': 'whatever-revision-path',
                     'KCIDB_CHECKOUT_ID': 'redhat:5678',
                     'KCIDB_BUILD_ID': 'redhat:1234'}


class TestKCIDBTestAdapter(unittest.TestCase):
    """Test cases for KCIDBTestAdapter class."""

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_artifacts_path(self):
        """Ensure artifacts_path works."""
        with create_temporary_kcidb() as kcidb_path:
            kcidb_data = KCIDBFile(kcidb_path)
        adapter = KCIDBTestAdapter(
            **{'output': 'whatever-dir', 'kcidb_data': kcidb_data, 'upload': False,
               'instance_no': 1})

        expected = '5678/1234/redhat:5678/build_x86_64_redhat:1234/tests/'
        self.assertEqual(expected, adapter.artifacts_path)


class TestRestraintStandaloneTest(unittest.TestCase):
    """Test cases for RestraintStandaloneTest class."""

    # pylint: disable=too-many-instance-attributes
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @freeze_time('2020-11-13T16:34:45.912589Z', tz_offset=1)
    def setUp(self) -> None:
        recipe_xml = (
            '<recipe>'
            '  <tasks>'
            '    <task id="1" name="a3" status="Completed" result="PASS">'
            '      <fetch url="git://"/>'
            '      <params>'
            '        <param name="CKI_ID" value="1"/>'
            '        <param name="CKI_NAME" value="a3"/>'
            '      </params>'
            '    </task>'
            '    <task id="2" name="a4" status="Completed">'
            '      <fetch url="git://"/>'
            '      <params>'
            '        <param name="CKI_ID" value="2"/>'
            '        <param name="CKI_UNIVERSAL_ID" value="some/path"/>'
            '      </params>'
            '    </task>'
            '  </tasks>'
            '</recipe>'
        )
        self.restraint_tasks = RestraintRecipe.create_from_string(recipe_xml).tasks
        self.host = Host({'hostname': 'hostname1', 'recipe_id': 1234, 'done_processing': True})
        self.host.counter = OutputDirCounter()
        self.host_no_retcode = Host({'hostname': 'hostname1', 'recipe_id': 1234})
        self.host_no_retcode.counter = OutputDirCounter()
        console_task = ConsoleTask(task_id=1, task_name='a3', result='PASS', status='Completed')
        self.task_result = TaskResult(self.host, self.restraint_tasks[0], console_task)
        self.task_result.kcidb_status = 'PASS'
        self.mock_adapter = mock.Mock()
        self.mock_adapter.checkout_id = '92b8f402aa964f209772e30190af5de818af996c'
        with create_temporary_kcidb() as kcidb_path:
            self.mock_adapter.kcidb_data = KCIDBFile(kcidb_path)
        self.mock_adapter.job_id = '1234'
        self.mock_adapter.build = KCIDB_DEFAULT_JSON['builds'][0]
        self.mock_adapter.tests = KCIDB_DEFAULT_JSON['tests']
        self.mock_adapter.upload = False
        self.mock_adapter.instance_no = 1

        self.standalone_test = RestraintStandaloneTest(self.mock_adapter, self.task_result,
                                                       testplan=False)
        console_task = ConsoleTask(task_id=1, task_name='a3', result='PASS', status='Completed')
        task_result_no_retcode = TaskResult(self.host_no_retcode,
                                            self.restraint_tasks[0],
                                            console_task)
        task_result_no_retcode.kcidb_status = 'PASS'
        self.standalone_test_no_retcode = RestraintStandaloneTest(self.mock_adapter,
                                                                  task_result_no_retcode,
                                                                  testplan=False)

        console_task = ConsoleTask(task_id=1, task_name='a3', result='WARN', status='Completed')
        task_result_normalize = TaskResult(self.host,
                                           self.restraint_tasks[0],
                                           console_task)
        task_result_normalize.kcidb_status = 'ERROR'
        self.standalone_test_normalize = RestraintStandaloneTest(self.mock_adapter,
                                                                 task_result_normalize,
                                                                 testplan=False)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    @mock.patch('upt.restraint.wrapper.misc.upload_file', mock.Mock())
    @mock.patch('upt.logger.LOGGER.debug')
    def test__prepare_output_file(self, mock_debug):
        """
        Regression test for issue#120

        Verify that the correct relative directory path is obtained
        from the output file path.
        """
        with create_temporary_kcidb() as kcidb_path:
            kcidb_data = KCIDBFile(kcidb_path)
            adapter = KCIDBTestAdapter(
                output=pathlib.Path('/path/to/output'),
                artifacts_path='/path/to/artifacts',
                instance_no=123,
                kcidb_data=kcidb_data,
                upload=True
            )
            standalone_test = RestraintStandaloneTest(
                adapter,
                self.task_result,
                testplan=False
            )
            output_file = pathlib.Path('/path/to/output/tasks/5/logs/harness.log')
            standalone_test._prepare_output_file(output_file)

            upload_destination = pathlib.Path(
                f'{os.environ["CI_PIPELINE_ID"]}/{os.environ["CI_JOB_ID"]}'
                f'/redhat:{os.environ["CI_PIPELINE_ID"]}/build_x86_64_redhat:'
                f'{os.environ["CI_JOB_ID"]}/tests/{adapter.instance_no}'
                '/results_0001/tasks/5/logs'
            )
            destination = '/path/to/output/tasks/5/logs'
            mock_debug.assert_called_with(
                'Uploading file: upload_destination=%s, file_name=%s, file_parent=%s',
                upload_destination,
                output_file.name,
                destination
            )

    @mock.patch('upt.restraint.wrapper.misc.upload_file')
    @mock.patch('upt.restraint.wrapper.task_result.TaskResult.output_files',
                new_callable=mock.PropertyMock)
    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_output_files_uploading_files(self, mock_output_files, mock_upload_file):
        """Ensure RestraintStandaloneTest output_files works when uploading files."""
        output_path = 'run/run.done'
        path = f'{output_path}/recipes/1234/tasks/1'
        full_path = f's3://my-dst-bucket/{path}'
        mock_adapter = self.mock_adapter
        mock_adapter.build_id = 'redhat:1'
        mock_adapter.upload = True
        mock_adapter.output = output_path
        mock_adapter.artifacts_path = 'something'

        mock_upload_file.side_effect = [f'{full_path}/file1', f'{full_path}/file2']

        files = [
            pathlib.Path(path, 'file1'),
            pathlib.Path(path, 'file2'),
        ]
        mock_output_files.return_value = files

        ret_obj = RestraintStandaloneTest(mock_adapter, self.task_result, testplan=False)
        ret_obj.testplan = False
        self.assertListEqual(
            ret_obj.output_files,
            [
                {'name': 'file1', 'url': f'{full_path}/file1'},
                {'name': 'file2', 'url': f'{full_path}/file2'},
            ]
        )

    @mock.patch('upt.restraint.wrapper.misc.convert_path_to_link')
    @mock.patch('upt.restraint.wrapper.task_result.TaskResult.output_files',
                new_callable=mock.PropertyMock)
    @mock.patch('upt.logger.LOGGER.debug', mock.Mock())
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_output_files_wihout_uploading_files(self, mock_output_files, mock_convert_link):
        """Ensure RestraintStandaloneTest output_files works when not uploading files."""
        output_path = 'run/run.done'
        path = f'{output_path}/recipes/1234/tasks/1'
        full_path = f's3://mybucket/{path}'
        mock_adapter = self.mock_adapter
        mock_adapter.build_id = 'redhat:1'
        mock_adapter.output = output_path
        mock_adapter.artifacts_path = 'something'

        mock_convert_link.side_effect = [f'{full_path}/file1', f'{full_path}/file2']
        files = [
            pathlib.Path(path, 'file1'),
            pathlib.Path(path, 'file2'),
        ]

        mock_output_files.return_value = files

        ret_obj = RestraintStandaloneTest(mock_adapter, self.task_result, testplan=False)
        self.assertListEqual(
            ret_obj.output_files,
            [
                {'name': 'file1', 'url': f'{full_path}/file1'},
                {'name': 'file2', 'url': f'{full_path}/file2'},
            ]
        )

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_duration(self):
        """Ensure _duration and finish_time work."""
        mock_start_time = "2020-11-09T18:06:48.872084Z"
        console_task = ConsoleTask(task_id=1, task_name='a3', result='PASS', status='Completed')
        task_result = TaskResult(self.host,
                                 self.restraint_tasks[0],
                                 console_task)
        delta = 5
        with mock.patch.object(task_result, 'start_time', mock_start_time):
            time_frozen_at = (date_parse(mock_start_time) + timedelta(seconds=delta)).isoformat()
            with freeze_time(time_frozen_at, tz_offset=1):
                standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result)
                self.assertEqual(standalone_test.duration, delta)
                self.assertEqual(standalone_test.finish_time, '2020-11-09T18:06:53.872084+00:00')

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render(self):
        """Ensure render works."""
        self.mock_adapter.output = 'run/run.done'

        with mock.patch.object(self.standalone_test, 'testplan', True):
            result = self.standalone_test.render()
            self.assertEqual(result['status'], 'PASS')
            self.assertEqual(result['start_time'], self.standalone_test.start_time)

        with mock.patch.object(self.standalone_test, 'testplan', False):
            result = self.standalone_test.render()
            self.assertEqual(result['misc']['fetch_url'], 'git://')
            self.assertEqual(result['environment'], {'comment': 'hostname1'})
            self.assertNotEqual(result['misc']['provenance'], [])

        with mock.patch.dict(os.environ, {'GITLAB_CI': 'false', 'BEAKER_URL': ''}):
            result = self.standalone_test.render()
            self.assertEqual(result['misc']['provenance'], [])

    @mock.patch('upt.restraint.wrapper.kcidb_adapter.RestraintStandaloneTest.output_files',
                new_callable=mock.PropertyMock)
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render_output_files(self, mock_output_files):
        """Ensure output_files are rendered."""
        output_files_base_path = 'https://s3.service.loc/bucket/base_path/recipes/1234/tasks/1'

        output_files = [
            {'name': 'file_1.log', 'url': f'{output_files_base_path}/logs/file_1.log'},
            {'name': 'file_2.log', 'url': f'{output_files_base_path}/logs/file_2.log'},
        ]

        mock_output_files.return_value = output_files

        result = self.standalone_test.render()
        self.assertCountEqual(output_files, result['output_files'])

    @mock.patch('upt.restraint.wrapper.kcidb_adapter.RestraintStandaloneTest._prepare_output_file')
    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_get_result(self, mock_output_files):
        """Ensure get_result works."""
        # Mock of the SubtaskResult
        mock_subtask_result = mock.Mock()
        mock_subtask_result.result_id = 1
        mock_subtask_result.name = 'result_name'
        mock_subtask_result.status = 'PASS'
        mock_subtask_result.output_files = [
            pathlib.Path(
                'artifacts/run.done.01/job0.1/recipes/1234/tasks/1/results/1/logs/file_1.log'
            ),
            pathlib.Path(
                'artifacts/run.done.01/job0.1/recipes/1234/tasks/1/results/1/logs/file_2.log'
            ),
        ]

        output_files_base_path = 'https://s3.service.loc/bucket/base_path/recipes/1234/tasks/1'

        output_files = [
            {'name': 'file_1.log', 'url': f'{output_files_base_path}/logs/file_1.log'},
            {'name': 'file_2.log', 'url': f'{output_files_base_path}/logs/file_2.log'},
        ]

        # Side effect for .prepare_output_file (subtask has two output files)
        mock_output_files.side_effect = output_files

        test_id = 'test_id'

        expected = {
            'id': f'{test_id}.{mock_subtask_result.result_id}',
            'name': mock_subtask_result.name,
            'output_files': output_files,
            'status': mock_subtask_result.status
        }

        result = self.standalone_test.get_result(mock_subtask_result, test_id)

        self.assertDictEqual(expected, result)

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_standalone_test_finish_time(self):
        """Ensure finish_time is set to beaker key."""
        console_task = ConsoleTask(task_id=1, task_name='a4', result='', status='Completed')
        task_result = TaskResult(self.host,
                                 self.restraint_tasks[1],
                                 console_task)
        standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result, testplan=False)
        standalone_test.finish_time = 'some'

        self.assertEqual('some', standalone_test.misc['beaker']['finish_time'])

    @mock.patch.dict(os.environ, DEF_ENV_MOCK_DICT)
    def test_render_path(self):
        """Ensure render sets path from univesal_id."""
        console_task = ConsoleTask(task_id=2, task_name='a4', result='', status='Completed')
        task_result = TaskResult(self.host, self.restraint_tasks[1], console_task)
        task_result.kcidb_status = 'PASS'
        standalone_test = RestraintStandaloneTest(self.mock_adapter, task_result, testplan=False)
        self.assertEqual('some.path', standalone_test.render()['path'])
