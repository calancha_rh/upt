"""Watch restraint files for subtask results and LWD hits."""
from pathlib import Path
from threading import Semaphore

from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

from upt.const import RSTRNT_JOB_XML
from upt.logger import LOGGER
from upt.logger import colorize
from upt.restraint.file import RestraintJobWatcher

from .actions import ActionOnResult


class Watcher:
    """Watch file changes in a directory and print subtask results."""

    def __init__(self):
        """Create the object."""
        self.observer = Observer()
        self.sem_lwd_sync = Semaphore(1)
        self.event_handler = Handler(self.sem_lwd_sync)

    def run(self, directory2watch):
        """Start watching a directory."""
        LOGGER.debug('* watching %s for subtask results', directory2watch)
        self.event_handler.init_job_watcher(Path(directory2watch, RSTRNT_JOB_XML))
        self.observer.schedule(self.event_handler, directory2watch, recursive=True)
        self.observer.start()
        self.observer.join()
        LOGGER.debug('* observer exiting')

    def get_job(self):
        """Get the latest job updates."""
        return self.event_handler.job_watcher.get_job()


class Handler(FileSystemEventHandler):
    """Process file changes."""

    def __init__(self, sem_lwd_sync):
        """Create the object."""
        super().__init__()
        # Ensures results aren't processed before event handler finishes
        self.sem_lwd_sync = sem_lwd_sync
        # Restraint Job Watcher
        self.job_watcher: RestraintJobWatcher

    def init_job_watcher(self, watched_file):
        """Init watcher file."""
        self.job_watcher = RestraintJobWatcher(watched_file)

    def flat_restraint_diff(self):
        """
        Flat diff to include results of a task.

        This method should be removed after refactoring restraint protocol
        """
        job_file = self.job_watcher.get_job()
        results = []
        if (job_diff := self.job_watcher.get_job_changes()):
            for recipeset_diff in job_diff.recipesets:
                for recipe_diff in recipeset_diff.recipes:
                    for task_diff in recipe_diff.tasks:
                        task = job_file.get_task_by_id(int(recipe_diff.id), int(task_diff.id))
                        if task.status not in ['New', 'Running'] and task.results:
                            results.append({
                                'recipe_id': int(recipe_diff.id),
                                'task_id': int(task_diff.id),
                                'lwd_hit': task.hit_localwatchdog,
                                'results': [
                                    {
                                        'name': result.path,
                                        'result': result.result,
                                    }
                                    for result in task.results
                                ]
                            })
        return results

    @staticmethod
    def print_console_messages(task):
        """Print console message for a result in a task and LWD hits."""
        if task['lwd_hit']:
            print(f'#{task["recipe_id"]}: task_id {task["task_id"]} hit LWD.')
        for result in task['results']:
            msg = ActionOnResult.format_msg(
                task['recipe_id'],
                task['task_id'],
                colorize(f'{result["result"]:<20}'),
                '.',
                result['name']
            )
            print(msg)

    def on_modified(self, event):
        """Handle changes in the job.xml file."""
        if RSTRNT_JOB_XML in event.src_path:
            with self.sem_lwd_sync:
                # Update watchdog
                self.job_watcher.update_info()
                LOGGER.debug('Updating restraint info from %s file', RSTRNT_JOB_XML)

                # Flat information and process it
                for task in self.flat_restraint_diff():
                    # Print console messages
                    self.print_console_messages(task)
