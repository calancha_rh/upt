"""Misc utility methods."""
import os
import pathlib
import typing
from urllib.parse import quote
from urllib.parse import unquote

from cki.kcidb.utils import upload_file
from twisted.internet import error

from upt.logger import LOGGER


def add_directory_suffix(output, instance_no):
    """Add numerical .done.XY suffix to directory."""
    dirname = f'{output}.done.{instance_no:02d}'
    assert not os.path.isdir(dirname)
    return dirname


def attempt_reactor_stop(reactor):
    """If twisted reactor is running, try to stop it."""
    if reactor.running:
        try:
            reactor.callFromThread(reactor.stop)
        except error.ReactorNotRunning:
            pass


def attempt_heartbeat_stop(heartbeat_loop):
    """Attempt to stop heartbeat looping call."""
    try:
        heartbeat_loop.stop()
    except (AssertionError,  AttributeError):
        pass


def convert_path_to_link(path: typing.Union[str, pathlib.Path], is_file: bool) -> str:
    """Get a quoted url to path, or just path if we're not running in pipeline."""
    if prefix := os.environ.get(f'UPT_ARTIFACT_{"FILE" if is_file else "DIRECTORY"}_URL_PREFIX'):
        return prefix + quote(str(path))
    return str(path)


def get_most_recent_task_result(recipe_id, task_id, task_results):
    """Get the most recent task state transition."""
    for task_result in reversed(task_results):
        if task_result.task_id == task_id and task_result.recipe_id == recipe_id:
            return task_result
    return None


def fix_file_name_encoded_as_html(path: pathlib.Path) -> pathlib.Path:
    """
    Clean file name encoded as HTML.

    When running restraint in standalone mode file names are HTML encoded.

    If we run restraint with beaker, file names are not HTML encoded.

    Example:
      * restraint standalone produces 'RFC%203961.log'
      * restraint with beaker produces 'RFC 3961.log'
    """
    file_name = path.name
    new_name = unquote(file_name, encoding='utf-8')
    if file_name != new_name:
        directory = path.parent
        return path.rename(pathlib.Path(directory, new_name))

    return path


def build_destination_url(adapter,
                          fpath: pathlib.Path,
                          relative_dir_path: pathlib.Path,
                          host_counter_path: typing.Optional[str] = None) -> str:
    """Prepare URL based on upload flag."""
    if adapter.upload:
        upload_destination = pathlib.Path(
            adapter.artifacts_path,
            str(adapter.instance_no),
            host_counter_path or "",
            relative_dir_path)
        LOGGER.debug('Uploading file: upload_destination=%s, file_name=%s, file_parent=%s',
                     upload_destination, fpath.name, str(fpath.parent))
        url = upload_file(adapter.visibility, upload_destination,
                          fpath.name, source_path=str(fpath))
    else:
        url = convert_path_to_link(fpath, True)
    return url
