"""Constants file."""

# Workaround! The reservesys task in Beaker started failing, this is the
# current solution, we are advised to use master branch and .tar.gz.
URL_RESERVESYS_TASK = 'https://github.com/beaker-project/beaker-core-tasks/' \
                      'archive/master.tar.gz#reservesys'

JOB_XML_STUB = """<?xml version="1.0" encoding="utf-8"?><job>
<recipeSet></recipeSet></job>"""

BKR_CORE_TASKS_URL = "https://github.com/beaker-project/beaker-core-tasks/" \
                     "archive/master.zip#{task}"

# 8 hours. Default duration of reservesys task if we cannot add-up duration of
# tasks for some reason and duration isn't forced.
DEFAULT_RESERVESYS_DURATION = 28800

# restraint commands to run tests
RSTRNT_CMDS = 'rstrnt_run_tests.sh'

# XML filename with tests to run using restraint
RSTRNT_JOB_XML = 'job.xml'

# Matches jobids in bkr job-submit/job-clone output
PATTERN_JOBIDS = '.*?(J:[0-9]+).*?'

# https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ec2.html#EC2.Instance.state
EC2_INSTANCE_RUNNING = 16
