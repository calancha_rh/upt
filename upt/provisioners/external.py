"""External provisioner."""
import os

from ruamel.yaml.scalarstring import PreservedScalarString

from upt.misc import fixup_or_delete_tasks_without_fetch
from upt.plumbing.interface import ProvisionerCore
from upt.restraint.file import RestraintJob


class External(ProvisionerCore):
    """External provisioner."""

    def __init__(self, dict_data=None):
        """Construct instances after deserialization."""
        super().__init__(dict_data=dict_data)
        self.evaluate_confirm_time = 0

    def provision(self, **kwargs):
        """Provision all hosts in all resource groups."""
        if (rgs_count := len(self.rgs)) != 1:
            raise Exception(f'only one resource group supported, got {rgs_count}')
        resource_group = self.rgs[0]
        resource_group.resource_id = '1'
        recipe_set = resource_group.recipeset
        if (host_count := len(recipe_set.hosts)) != 1:
            raise Exception(f'only one host per recipeset supported, got {host_count}')

        restraint_job = RestraintJob.create_from_string(recipe_set.restraint_xml)
        restraint_recipes = restraint_job.get_all_recipes()
        if (recipe_count := len(restraint_recipes)) != 1:
            raise Exception(f'only one recipe per recipeset supported, got {recipe_count}')
        restraint_recipes[0].id = '1'
        recipe_set.hosts[0].recipe_id = 1
        recipe_set.hosts[0].hostname = os.environ['UPT_EXTERNAL_HOST_IP']

        fixup_or_delete_tasks_without_fetch(restraint_job)
        recipe_set.restraint_xml = PreservedScalarString(str(restraint_job))

    def is_provisioned(self, resource_group):
        """Check if resource group is finished provisioning."""
        return True

    def reprovision_aborted(self, resource_group):
        """Provision a resource again, if provisioning failed."""
        # nothing to do here as provisioning cannot fail

    def get_resource_ids(self):
        """Return identifiers of resources provisioned during script run."""
        raise Exception('not implemented, as this is not used anywhere')

    def heartbeat(self, resource_group, recipe_ids_dead):
        """Check if resource is OK (provisioned, not broken/aborted)."""
        # nothing to do here

    def release_rg(self, resource_group):
        """Release all resources provisioned in a single resource group."""
        # nothing to do here

    def update_provisioning_request(self, resource_group):
        """Ensure that request file has up-to-date info after provisioning."""
        # nothing to do here
